/*
    libnono:  A simple nonogram library
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file
 * @author Taylor C. Richberger <taywee@gmx.com>
 * @brief Maintains groups of marked blocks, owned by nono_row.
 */

#pragma once

#include "types.h"

/**
 * Get zeroed nono_group
 * @returns nono_group with all zero fields
 */
extern struct nono_group *nono_group_new(nono_row_width_t start, nono_row_width_t size);

/**
 * Frees a group.
 *
 * @param group the group to free
 */
extern void nono_group_free(struct nono_group *group);

/**
 * Get the start point of this group in the row.
 *
 * @param group the group to access
 * @returns the start point, 0-indexed, of the group
 */
extern nono_row_width_t nono_group_start(const struct nono_group *group);

/**
 * Get the size of this group in the row.
 *
 * @param group the group to access
 * @returns the size of the group
 */
extern nono_row_width_t nono_group_size(const struct nono_group *group);
