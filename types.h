/*
    libnono:  A simple nonogram library
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file
 * @author Taylor C. Richberger <taywee@gmx.com>
 * @brief Common types.
 */

#pragma once

typedef signed int nono_row_groupcount_t;
typedef signed int nono_row_width_t;

struct nono_group;
struct nono_row;
struct nono_puzzle;

/** Error conditions */
enum nono_error
{
    NONO_ERROR_NONE,            /** No error */
    NONO_ERROR_UNEVEN_ROWS,     /** Rows given are not all the same length */
    NONO_ERROR_ZERO             /** A row or column dimension is zero */
};

/** Block type for solutions */
enum nono_block
{
    NONO_BLOCK_BLANK,   /** A blank, unmarked block */
    NONO_BLOCK_X,       /** A block marked to be forced blank, an X */
    NONO_BLOCK_MARK     /** A block marked as part of a group */
};
