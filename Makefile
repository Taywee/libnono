VERSION=$(shell git describe --abbrev=4 --dirty --always --tags)
CC=cc
AR=ar
DESTDIR=/usr/local
CFLAGS=-c -ansi -Wall -Wextra -pedantic -Wmissing-prototypes -Wstrict-prototypes -Wold-style-definition -std=c99 -MMD -MP -O0 -fPIC -ggdb -D_POSIX_C_SOURCE=200809L
LFLAGS=-ggdb
SOURCES=puzzle.c group.c row.c
OBJECTS=$(SOURCES:.c=.o)
DEPENDENCIES=$(SOURCES:.c=.d)
LIBNAME=libnono
LIBS=$(LIBNAME).a $(LIBNAME).so

.PHONY: all clean install uninstall doc

all: $(LIBS) libnono.pc

test: test.o $(LIBS)
	$(CC) $(LFLAGS) -o $@ $< -I. $(LIBNAME).a

test.o: test.c
	$(CC) $(CFLAGS) -o $@ $< -I.

-include $(DEPENDENCIES)

libnono.so: $(OBJECTS)
	$(CC) $(LFLAGS) $^ -shared -o $@

libnono.a: $(OBJECTS)
	$(AR) rcs $@ $?

libnono.pc: libnono.pc.pre
	sed -e 's/MAKEFILE_VERSION/$(subst /,\/,$(VERSION))/g' \
		-e 's/MAKEFILE_PREFIX/$(subst /,\/,$(DESTDIR))/' libnono.pc.pre > libnono.pc

install: $(LIBS) libnono.pc
	install -d $(DESTDIR)/lib
	install -d $(DESTDIR)/include/libnono
	install -d $(DESTDIR)/lib/pkgconfig
	install $^ $(DESTDIR)/lib
	install *.h $(DESTDIR)/include/libnono
	install libnono.pc $(DESTDIR)/lib/pkgconfig

uninstall:
	-cd $(DESTDIR)/lib; rm $(LIBS)
	-rm -r $(DESTDIR)/include/libnono
	-rm $(DESTDIR)/lib/pkgconfig/libnono.pc

doc: Doxyfile
	-rm -r doc
	doxygen

Doxyfile: Doxyfile.pre
	sed -e 's/MAKEFILE_VERSION/$(subst /,\/,$(VERSION))/g' \
		-e 's/MAKEFILE_PREFIX/$(subst /,\/,$(DESTDIR))/' Doxyfile.pre > Doxyfile

clean :
	rm $(LIBS) $(OBJECTS) $(DEPENDENCIES) libnono.pc

%.o: %.c
	$(CC) $(CFLAGS) $(DEFINES) $< -o $@
