/*
    libnono:  A simple nonogram library
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "puzzle.h"

#include <string.h>
#include <stdio.h>

#include "group.h"
#include "row.h"

#define getgrid(grid, width, x, y) grid[(y) * (width) + (x)]

/** A struct that represents the puzzle */
struct nono_puzzle
{
    char * name;
    char * hidden_name;
    nono_row_width_t width;
    nono_row_width_t height;
    bool *solution;             /** Buffer containing the entire solved puzzle */
    enum nono_block *progress;  /** Buffer containing the in-progress solution */
    struct nono_row **rows;
    struct nono_row **columns;
};

#define tryset(target, value) if (target) { *target = value; }
#define errorout(target, value) tryset(target, value); return NULL;

struct nono_puzzle *nono_puzzle_new(const nono_row_width_t width, const nono_row_width_t height, const char *name, const char *hidden_name)
{
    struct nono_puzzle *output = calloc(1, sizeof(struct nono_puzzle));
    output->name = strdup(name);
    output->hidden_name = strdup(hidden_name);
    output->solution = calloc(width * height, sizeof(const bool));
    output->progress = calloc(width * height, sizeof(enum nono_block));
    output->rows = calloc(height, sizeof(struct nono_row *));
    output->columns = calloc(width, sizeof(struct nono_row *));
    output->width = width;
    output->height = height;
    return output;
}

void nono_puzzle_free(struct nono_puzzle *puzzle)
{
    if (!puzzle)
    {
        return;
    }
    free(puzzle->name);
    free(puzzle->hidden_name);
    free(puzzle->solution);
    free(puzzle->progress);

    for (nono_row_width_t i = 0; i < puzzle->height; ++i)
    {
        nono_row_free(puzzle->rows[i]);
    }
    free(puzzle->rows);
    for (nono_row_width_t i = 0; i < puzzle->width; ++i)
    {
        nono_row_free(puzzle->columns[i]);
    }
    free(puzzle->columns);
    free(puzzle);
}

void nono_puzzle_overscan(bool *puzzle, nono_row_width_t *width, nono_row_width_t *height)
{
    nono_row_width_t overscanL = 0;
    nono_row_width_t overscanR = 0;
    nono_row_width_t overscanT = 0;
    nono_row_width_t overscanB = 0;

    // Left
    for (nono_row_width_t column = 0; column < *width; ++column)
    {
        bool blank = true;
        for (nono_row_width_t row = 0; row < *height; ++row)
        {
            if (getgrid(puzzle, *width, column, row))
            {
                blank = false;
                break;
            }
        }
        if (blank)
        {
            ++overscanL;
        } else
        {
            break;
        }
    }

    // Right
    for (nono_row_width_t column = *width - 1; column >= 0; --column)
    {
        bool blank = true;
        for (nono_row_width_t row = 0; row < *height; ++row)
        {
            if (getgrid(puzzle, *width, column, row))
            {
                blank = false;
                break;
            }
        }
        if (blank)
        {
            ++overscanR;
        } else
        {
            break;
        }
    }

    // Top
    for (nono_row_width_t row = 0; row < *height; ++row)
    {
        bool blank = true;
        for (nono_row_width_t column = 0; column < *width; ++column)
        {
            if (getgrid(puzzle, *width, column, row))
            {
                blank = false;
                break;
            }
        }
        if (blank)
        {
            ++overscanT;
        } else
        {
            break;
        }
    }

    // Bottom
    for (nono_row_width_t row = *height - 1; row >= 0; --row)
    {
        bool blank = true;
        for (nono_row_width_t column = 0; column < *width; ++column)
        {
            if (getgrid(puzzle, *width, column, row))
            {
                blank = false;
                break;
            }
        }
        if (blank)
        {
            ++overscanB;
        } else
        {
            break;
        }
    }
    // If *width == overscanL, the whole puzzle is blank anyway.  If it is not, there is at least one block, and the puzzle is not blank
    if (*width == overscanL)
    {
        *width = 0;
        *height = 0;
        return;
    }

    const nono_row_width_t newwidth = *width - (overscanL + overscanR);
    const nono_row_width_t newheight = *height - (overscanT + overscanB);

    size_t pos = 0;
    for (nono_row_width_t row = 0; row < newheight; ++row)
    {
        for (nono_row_width_t column = 0; column < newwidth; ++column)
        {
            puzzle[pos++] = getgrid(puzzle, *width, column + overscanL, row + overscanT);
        }
    }
    *width = newwidth;
    *height = newheight;
}

struct nono_puzzle *nono_puzzle_load_ascii(const char * puzzle, enum nono_error *error, size_t size, const char mark, const char rowend)
{
    const char * name_start = puzzle;
    const char * name_end = strchr(name_start, '\n');
    const size_t name_size = name_end - puzzle;
    char name[name_size + 1];
    memcpy(name, name_start, name_size);
    name[name_size] = '\0';
    const char * hidden_name_start = name_end + 1;
    const char * hidden_name_end = strchr(hidden_name_start, '\n');
    const size_t hidden_name_size = hidden_name_end - hidden_name_start;
    char hidden_name[hidden_name_size + 1];
    memcpy(hidden_name, hidden_name_start, hidden_name_size);
    hidden_name[hidden_name_size] = '\0';

    puzzle = hidden_name_end + 1;
    if (!size)
    {
        size = strlen(puzzle);
    }

    // Early trim blank lines at end
    while (size > 0 && puzzle[size - 1] == rowend)
    {
        --size;
    }

    // Early trim blank lines at beginning
    while (size > 0 && puzzle[0] == rowend)
    {
        ++puzzle;
        --size;
    }

    // Error out if size is 0 after the trim
    if (!size)
    {
        errorout(error, NONO_ERROR_ZERO);
    }

    nono_row_width_t rowlen = 0;
    nono_row_width_t rows = 0;
    nono_row_width_t thisrowlen = 0;

    // The <= size is used because the end of the string is a special case treated as a line ending
    for (size_t i = 0; i <= size; ++i)
    {
        const char this = puzzle[i];
        // The last row will not be terminated in a newline, due to the early trim
        if (this == rowend || i == size)
        {
            if (thisrowlen > rowlen)
            {
                rowlen = thisrowlen;
            }
            ++rows;
            thisrowlen = 0;
        } else
        {
            ++thisrowlen;
        }
    }

    // Uncropped puzzle, for the full puzzle scan
    bool boolpuzzle[rows * rowlen];

    size_t pos = 0;
    for (size_t i = 0; i <= size; ++i)
    {
        const char this = puzzle[i];
        if (this == rowend || i == size)
        {
            while (thisrowlen < rowlen)
            {
                boolpuzzle[pos++] = false;
                ++thisrowlen;
            }
            thisrowlen = 0;
        } else
        {
            boolpuzzle[pos++] = this == mark;
            ++thisrowlen;
        }
    }

    nono_puzzle_overscan(boolpuzzle, &rowlen, &rows);

    return nono_puzzle_read(boolpuzzle, error, rowlen, rows, true, name, hidden_name);
}

struct nono_puzzle *nono_puzzle_read(const bool * puzzle, enum nono_error *error, const nono_row_width_t width, const nono_row_width_t height, const bool rowmajor, const char *name, const char *hidden_name)
{
    *error = NONO_ERROR_NONE;
    if (width == 0 || height == 0)
    {
        errorout(error, NONO_ERROR_ZERO);
    }
    struct nono_puzzle *output = nono_puzzle_new(width, height, name, hidden_name);

    size_t i = 0;
    for (nono_row_width_t y = 0; y < height; ++y)
    {
        for (nono_row_width_t x = 0; x < width; ++x, ++i)
        {

            if (rowmajor)
            {
                output->solution[i] = puzzle[(y * width) + x];
            } else
            {
                output->solution[i] = puzzle[(x * height) + y];
            }
        }
    }

    for (nono_row_width_t y = 0; y < height; ++y)
    {
        output->rows[y] = nono_row_new(output->solution + (y * width), width);
    }

    for (nono_row_width_t x = 0; x < width; ++x)
    {
        bool buffer[height];

        for (nono_row_width_t y = 0; y < height; ++y)
        {
            buffer[y] = output->solution[(y * width) + x];
        }

        output->columns[x] = nono_row_new(buffer, height);
    }
    return output;
}

nono_row_width_t nono_puzzle_width(const struct nono_puzzle *puzzle)
{
    return puzzle->width;
}

nono_row_width_t nono_puzzle_height(const struct nono_puzzle *puzzle)
{
    return puzzle->height;
}

const struct nono_row *nono_puzzle_column(const struct nono_puzzle *puzzle, nono_row_width_t n)
{
    return puzzle->columns[n];
}

const struct nono_row *nono_puzzle_row(const struct nono_puzzle *puzzle, nono_row_width_t n)
{
    return puzzle->rows[n];
}

const char *nono_puzzle_name(const struct nono_puzzle *puzzle)
{
    return puzzle->name;
}

const char *nono_puzzle_hidden_name(const struct nono_puzzle *puzzle)
{
    return puzzle->hidden_name;
}

bool nono_puzzle_answer(const struct nono_puzzle *puzzle, const nono_row_width_t column, const nono_row_width_t row)
{
    return getgrid(puzzle->solution, puzzle->width, column, row);
}

enum nono_block nono_puzzle_progress(const struct nono_puzzle *puzzle, const nono_row_width_t column, const nono_row_width_t row)
{
    return getgrid(puzzle->progress, puzzle->width, column, row);
}

void nono_puzzle_mark(struct nono_puzzle *puzzle, const nono_row_width_t column, const nono_row_width_t row, const enum nono_block block)
{
    getgrid(puzzle->progress, puzzle->width, column, row) = block;
}

extern bool nono_puzzle_solved(const struct nono_puzzle *puzzle)
{
    const size_t bufsize = puzzle->width * puzzle->height;
    for (size_t i = 0; i < bufsize; ++i)
    {
        // enter if solution == true and progress != mark, or if solution ==
        // false and progress == mark.  In other words, if there are any wrong
        // marks, or if there are insufficient marks, the puzzle is incorrect.
        if (puzzle->solution[i] != (puzzle->progress[i] == NONO_BLOCK_MARK))
        {
            return false;
        }
    }
    return true;
}
