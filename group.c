/*
    libnono:  A simple nonogram library
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "group.h"

#include <stdlib.h>
#include <stdbool.h>

/** block group */
struct nono_group
{
    nono_row_width_t start;   /** start point of the group, 0-indexed */
    nono_row_width_t size;   /** number of blocks in the group */
};

struct nono_group *nono_group_new(nono_row_width_t start, nono_row_width_t size)
{
    struct nono_group *output = malloc(sizeof(struct nono_group));
    output->start = start;
    output->size = size;
    return output;
}

void nono_group_free(struct nono_group *group)
{
    free(group);
}

nono_row_width_t nono_group_start(const struct nono_group *group)
{
    return group->start;
}

nono_row_width_t nono_group_size(const struct nono_group *group)
{
    return group->size;
}
