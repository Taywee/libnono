/*
    libnono:  A simple nonogram library
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "row.h"

#include <stdlib.h>

#include "group.h"

/** row or column */
struct nono_row
{
    nono_row_groupcount_t groupcount;  /** start point of the group, 0-indexed */
    struct nono_group **groups;  /** number of block groups in row */
};

struct nono_row *nono_row_new(const bool * row, const nono_row_width_t size)
{
    struct nono_row *output = calloc(1, sizeof(struct nono_row));
    bool ongroup = false;
    nono_row_groupcount_t groupcount = 0;
    for (nono_row_width_t i = 0; i < size; ++i)
    {
        if(row[i])
        {
            if (!ongroup)
            {
                ongroup = true;
                ++groupcount;
            }
        } else
        {
            ongroup = false;
        }
    }
    output->groupcount = groupcount;
    output->groups = calloc(groupcount, sizeof(struct nono_group *));

    nono_row_groupcount_t groupnum = 0;
    nono_row_width_t groupsize = 0;
    nono_row_width_t groupstart = 0;
    ongroup = false;

    for (nono_row_width_t i = 0; i <= size; ++i)
    {
        if(i < size && row[i])
        {
            if (ongroup)
            {
                ++groupsize;
            } else
            {
                groupsize = 1;
                groupstart = i;
                ongroup = true;
            }
        } else
        {
            if (ongroup)
            {
                output->groups[groupnum++] = nono_group_new(groupstart, groupsize);
                ongroup = false;
            }
        }
    }
    return output;
}

void nono_row_free(struct nono_row *row)
{
    if (!row)
    {
        return;
    }
    for (nono_row_groupcount_t i = 0; i < row->groupcount; ++i)
    {
        nono_group_free(row->groups[i]);
    }
    free(row->groups);
    free(row);
}

nono_row_groupcount_t nono_row_groupcount(const struct nono_row *row)
{
    return row->groupcount;
}

const struct nono_group *nono_row_group(const struct nono_row *row, nono_row_groupcount_t n)
{
    return (row->groups[n]);
}
