# libnono
Simple nonogram library.

Nonograms are picture crossword puzzles (more famously known by the names
Griddlers and Picross).  There is plenty of information about them available on
Wikipedia's Nonogram page.

This is built to facilitate the development of efficient nonogram applications
so that they may have a light front-end without focusing on the nonogram logic,
and to allow various different applications targetting various platforms and
UIs to use a common codebase backend and have the same features available.

The source code will be doxygenated, so API docs will be generated from the
sources when they are finished.

# Author
libnono is Copyright (C) 2015 Taylor C. Richberger

This library is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
