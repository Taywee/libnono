#include <stdio.h>

#include <nono.h>

int main(int argc, char **argv)
{
    FILE *file = fopen(argv[1], "r");
    if (file)
    {
        if (!fseek(file, 0, SEEK_END))
        {
            const long size = ftell(file);
            char buffer[size];

            if (fseek(file, 0, SEEK_SET))
            {
                fputs("Could not set to end of file\n", stderr);
                return 1;
            }

            fread(buffer, sizeof(char), size, file);

            enum nono_error error;
            struct nono_puzzle *puzzle = nono_puzzle_load_ascii(buffer, &error, size, 'X', '\n');
            if (error == NONO_ERROR_NONE)
            {
                const nono_row_width_t width = nono_puzzle_width(puzzle);
                const nono_row_width_t height = nono_puzzle_height(puzzle);
                printf("Name: %s\nHidden Name: %s\n", nono_puzzle_name(puzzle), nono_puzzle_hidden_name(puzzle));
                printf("Width: %d\nHeight: %d\n", width, height);

                puts("Rows:");
                for (nono_row_width_t y = 0; y < height; ++y)
                {
                    printf("%2d: ", y);

                    const struct nono_row *row = nono_puzzle_row(puzzle, y);
                    const nono_row_groupcount_t groupcount = nono_row_groupcount(row);
                    if (groupcount > 0)
                    {
                        for (nono_row_groupcount_t groupnum = 0; groupnum < groupcount; ++groupnum)
                        {
                            const struct nono_group *group = nono_row_group(row, groupnum);
                            printf("%2d ", nono_group_size(group));
                        }
                        puts("");
                    } else
                    {
                        puts(" 0");
                    }
                }

                puts("Columns:");
                for (nono_row_width_t x = 0; x < width; ++x)
                {
                    printf("%2d: ", x);

                    const struct nono_row *column = nono_puzzle_column(puzzle, x);
                    const nono_row_groupcount_t groupcount = nono_row_groupcount(column);
                    if (groupcount > 0)
                    {
                        for (nono_row_groupcount_t groupnum = 0; groupnum < groupcount; ++groupnum)
                        {
                            const struct nono_group *group = nono_row_group(column, groupnum);
                            printf("%2d ", nono_group_size(group));
                        }
                        puts("");
                    } else
                    {
                        puts(" 0");
                    }
                }

                puts("Testing unmarked solution");
                printf("%d\n", nono_puzzle_solved(puzzle));
                puts("Marking solution");
                for (nono_row_width_t y = 0; y < height; ++y)
                {
                    for (nono_row_width_t x = 0; x < width; ++x)
                    {
                        nono_puzzle_mark(puzzle, x, y, nono_puzzle_answer(puzzle, x, y) ? NONO_BLOCK_MARK : NONO_BLOCK_BLANK);
                    }
                }
                puts("Testing correct solution");
                printf("%d\n", nono_puzzle_solved(puzzle));
                puts("Making wrong mark");
                nono_puzzle_mark(puzzle, 1, 0, NONO_BLOCK_MARK);
                puts("Testing wrong solution");
                printf("%d\n", nono_puzzle_solved(puzzle));
                puts("Marking solution");
                for (nono_row_width_t y = 0; y < height; ++y)
                {
                    for (nono_row_width_t x = 0; x < width; ++x)
                    {
                        nono_puzzle_mark(puzzle, x, y, nono_puzzle_answer(puzzle, x, y) ? NONO_BLOCK_MARK : NONO_BLOCK_BLANK);
                    }
                }
                puts("Testing correct solution");
                printf("%d\n", nono_puzzle_solved(puzzle));
                puts("Unmarking wrong mark");
                nono_puzzle_mark(puzzle, 0, 0, NONO_BLOCK_BLANK);
                puts("Testing wrong solution");
                printf("%d\n", nono_puzzle_solved(puzzle));

                nono_puzzle_free(puzzle);
            }
        }
    }
    return 0;
}
