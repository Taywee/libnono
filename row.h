/*
    libnono:  A simple nonogram library
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file
 * @author Taylor C. Richberger <taywee@gmx.com>
 * @brief Maintains a row of groups.  Owned by nono_puzzle.
 */
#pragma once

#include <stdbool.h>
#include <stdlib.h>

#include "types.h"
#include "group.h"

/**
 * Create a new row from the bool array given.
 *
 * @param row the array scanned to generate groups and such
 * @param size the size of row
 * @returns the row generated
 */
extern struct nono_row *nono_row_new(const bool * row, const nono_row_width_t size);

/**
 * Frees a row and all its held groups.
 *
 * @param row the row to free
 */
extern void nono_row_free(struct nono_row *row);

/**
 * Gets the count of `nono_group`s owned.
 *
 * @param row the row to get the group count of
 * @returns the number of groups owned by this row
 */
extern nono_row_groupcount_t nono_row_groupcount(const struct nono_row *row);

/**
 * Gets a single `nono_group` from this row.
 *
 * @param row the row to get the group from
 * @returns the group in question
 */
extern const struct nono_group *nono_row_group(const struct nono_row *row, nono_row_groupcount_t n);
