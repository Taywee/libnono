/*
    libnono:  A simple nonogram library
    Copyright (C) 2015 Taylor C. Richberger <taywee@gmx.com>

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file
 * @author Taylor C. Richberger <taywee@gmx.com>
 * @brief Maintains an entire puzzle.  This is usually the top-level type you
 * need to work directly with in most cases.
 */
#pragma once

#include <stdlib.h>
#include <stdbool.h>

#include "types.h"

/**
 * Creates an empty nono_puzzle.
 *
 * The rows and columns are pre-created, but none of the structs are built.
 *
 * @param width the width of the puzzle
 * @param height the height of the puzzle
 * @param name null-terminated string to the name of the puzzle
 * @param hidden_name null-terminated string to the hidden name of the puzzle
 */
extern struct nono_puzzle *nono_puzzle_new(const nono_row_width_t width, const nono_row_width_t height, const char *name, const char *hidden_name);

/**
 * Frees a puzzle and all associated resources.
 *
 * Also frees all rows and groups.
 *
 * @param puzzle puzzle to free.  NULL is a noop.
 */
extern void nono_puzzle_free(struct nono_puzzle *puzzle);

/**
 * Modifies a puzzle in place, removing underscan (ie. all blank edge rows and columns)
 *
 * New width and height are placed over the old ones.
 *
 * @param puzzle the puzzle to strip underscan from.  Is modified in place.
 * @param width the width of the puzzle that is fed in.  Is modified in place.
 * @param height the height of the puzzle that is fed in.  Is modified in place.
 */
extern void nono_puzzle_overscan(bool *puzzle, nono_row_width_t *width, nono_row_width_t *height);

/**
 * read an ascii puzzle with embedded newlines (which may be any single character) and returns a malloc'd structure.
 *
 * Returns null under any error conditions and sets error.  Calls
 * nono_puzzle_read.  This also trims all blank edge lines and does some other
 * optimizations to give a nice and clean puzzle to nono_puzzle_read.
 *
 * @param puzzle a buffer containing the puzzle structure.  All necessary data is copied before this call returns, so the buffer is not claimed
 * @param error if not NULL, set to a nono_error in the case of an error and otherwise reset to NONO_ERROR_NONE
 * @param size the length of the puzzle buffer.  If size is 0, puzzle should be null-terminated
 * @param mark the char in puzzle that indicates that a cell is marked.  All others are considered empty
 * @param rowend the character indicating the end of a row
 */
extern struct nono_puzzle *nono_puzzle_load_ascii(const char * puzzle, enum nono_error *error, size_t size, const char mark, const char rowend);

/**
 * Reads a puzzle in a boolean array with a width and height
 *
 * Returns null under any error conditions and sets error.
 *
 * @param puzzle a buffer containing the puzzle structure.  All necessary data is copied before this call returns, so the buffer is not claimed.
 * @param error if not NULL, set to a nono_error in the case of an error and otherwise reset to NONO_ERROR_NONE
 * @param width the width of the input puzzle
 * @param height the height of the input puzzle
 * @param rowmajor true if the puzzle is stored row-major, false if it is stored column-major.
 * @param name null-terminated string to the name of the puzzle
 * @param hidden_name null-terminated string to the hidden name of the puzzle
 */
extern struct nono_puzzle *nono_puzzle_read(const bool * puzzle, enum nono_error *error, const nono_row_width_t width, const nono_row_width_t height, const bool rowmajor, const char *name, const char *hidden_name);

/**
 * Get the puzzle's width.
 *
 * @param puzzle the puzzle to get width from
 * @returns the puzzle's width
 */
extern nono_row_width_t nono_puzzle_width(const struct nono_puzzle *puzzle);

/**
 * Get the puzzle's height.
 *
 * @param puzzle the puzzle to get height from
 * @returns the puzzle's height
 */
extern nono_row_width_t nono_puzzle_height(const struct nono_puzzle *puzzle);

/**
 * Get a column from the puzzle.
 *
 * @param puzzle the puzzle to get column from
 * @returns the puzzle column
 */
extern const struct nono_row *nono_puzzle_column(const struct nono_puzzle *puzzle, nono_row_width_t n);

/**
 * Get a row from the puzzle.
 *
 * @param puzzle the puzzle to get row from
 * @returns the puzzle row
 */
extern const struct nono_row *nono_puzzle_row(const struct nono_puzzle *puzzle, nono_row_width_t n);

/**
 * Get the puzzle's public name
 *
 * @param puzzle the puzzle to get public name from.
 * @returns the puzzle name
 */
extern const char *nono_puzzle_name(const struct nono_puzzle *puzzle);

/**
 * Get the puzzle's hidden name
 *
 * @param puzzle the puzzle to get hidden name from.
 * @returns the puzzle hidden name
 */
extern const char *nono_puzzle_hidden_name(const struct nono_puzzle *puzzle);

/**
 * Get a single block from the puzzle's solution.
 *
 * @param puzzle the puzzle to get the solution block from
 * @param column the X dimension
 * @param row the Y dimension
 * @returns the solution block
 */
extern bool nono_puzzle_answer(const struct nono_puzzle *puzzle, const nono_row_width_t column, const nono_row_width_t row);

/**
 * Get a single block from the puzzle's in-progress solving buffer
 *
 * @param puzzle the puzzle to get the solution block from
 * @param column the X dimension
 * @param row the Y dimension
 * @returns the in-progress block as marked
 */
extern enum nono_block nono_puzzle_progress(const struct nono_puzzle *puzzle, const nono_row_width_t column, const nono_row_width_t row);

/**
 * Mark a single block in the puzzle's in-progress solving buffer
 *
 * @param puzzle the puzzle in which to set the solution block
 * @param column the X dimension
 * @param row the Y dimension
 * @param block The status of the new mark
 */
extern void nono_puzzle_mark(struct nono_puzzle *puzzle, const nono_row_width_t column, const nono_row_width_t row, const enum nono_block block);

/**
 * Check if the puzzle is solved, through nono_puzzle_mark.
 *
 * NONO_BLOCK_X is equivalent to blank.
 *
 * @param puzzle the puzzle to check.
 * @returns true if the puzzle is solved, and false if the puzzle is still unsolved
 */
extern bool nono_puzzle_solved(const struct nono_puzzle *puzzle);
